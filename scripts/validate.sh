#!/usr/bin/env bash

os=""
case $(uname -a) in
  Linux*) os="linux" ;;
  Darwin*) os="mac" ;;
  *)
    echo "unsupported os"
    exit 1
    ;;
esac

if [ ! -d json-schema-spec ]; then
  git clone --branch draft-07 https://github.com/json-schema-org/json-schema-spec.git || exit 1

  # Since json-schema-spec does not have version tags, we check out to the latest
  # commit on draft-07 as a form of pinning.
  cd json-schema-spec || exit 1
  git checkout --detach 6e2b42516dc7e8845c980d284c61bd44c9f95cd2 || exit 1
  cd .. || exit 1
fi

status=0

for file in src/*.json; do
  echo -n "$file: "
  bin/validate-$os <"$file" || status=1
done

exit "$status"
