#!/bin/bash

set -e

PROJECT_DIR="$(dirname "$(realpath "$0")")/.."
SECURE_REPORT_FORMAT_SRC=$(realpath "$PROJECT_DIR/src/security-report-format.json")
SECURE_REPORT_FORMAT_DAST_DIST=$(realpath "$PROJECT_DIR/dist/dast-report-format.json")
PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/security-report-schemas"}

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/error-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/changelog-utils.sh"

# report_schema_version returns the version of a JSON report schema.
report_schema_version() {
  local schema_file="$1"

  if ! [[ -f "$schema_file" ]]; then
    error "Unable to find $schema_file."
  fi

  local version
  version=$(jq -r -e ".self.version" "$schema_file")

  if [[ -z "$version" ]]; then
    error "Aborting, unable to determine the latest self.version in $schema_file."
  fi

  echo "v$version"
}

# compare_schema_content_with_dist compares the content of a JSON schema
# with the content of its distribution, and fails if they are different.
compare_schema_content_with_dist() {
  local version="$1"
  local report_type="$2"
  local temp_file="$TMPDIR/$report_type.schema.json"

  curl --silent --fail --output "$temp_file" "$PROJECT_URL/-/raw/$version/dist/$report_type-report-format.json"

  # temporarily disable error handling, so we can display a better error message
  set +e
  diff -u "$temp_file" "dist/$report_type-report-format.json"
  local comparison_result="$?"
  set -e

  if [[ "$comparison_result" != "0" ]]; then
    echo
    error "The released version of the $report_type schema differs from what is in the dist directory, yet" \
      "the versions are the same." \
      "Did you change the schema, and forget to increment the version?"
  fi
}

VERSION_ALREADY_RELEASED=false
CHANGELOG_VERSION=$(changelog_last_version)
SRC_VERSION=$(report_schema_version "$SECURE_REPORT_FORMAT_SRC")

# All schemas ready for distribution will have the same version, DAST is used as an example to represent all.
DAST_DIST_VERSION=$(report_schema_version "$SECURE_REPORT_FORMAT_DAST_DIST")

# If curl returns a 404, we know the version has not been released.
if curl -X HEAD --silent --fail --output /dev/null "$PROJECT_URL/-/releases/$CHANGELOG_VERSION"; then
  VERSION_ALREADY_RELEASED=true
fi

echo "Version found in CHANGELOG:          $CHANGELOG_VERSION"
echo "Version found in src/:               $SRC_VERSION"
echo "Version found in dist/:              $DAST_DIST_VERSION"
echo "Changelog version already released:  $VERSION_ALREADY_RELEASED"
echo

if [[ "$CHANGELOG_VERSION" != "$SRC_VERSION" ]]; then
  error "Aborting, the version in the CHANGELOG and the source JSON have diverged." \
    "Do you need to add a CHANGELOG entry, or did you forget to update self.version?"
fi

if [[ "$SRC_VERSION" != "$DAST_DIST_VERSION" ]]; then
  error "Aborting, the version in the source JSON and the version in the distributed JSON have diverged." \
    "Please make sure self.version is up-to-date."
fi

if [[ "$VERSION_ALREADY_RELEASED" == "true" ]]; then
  compare_schema_content_with_dist "$SRC_VERSION" "container-scanning"
  compare_schema_content_with_dist "$SRC_VERSION" "dast"
  compare_schema_content_with_dist "$SRC_VERSION" "sast"
  compare_schema_content_with_dist "$SRC_VERSION" "dependency-scanning"
  compare_schema_content_with_dist "$SRC_VERSION" "secret-detection"
  compare_schema_content_with_dist "$SRC_VERSION" "coverage-fuzzing"
fi

echo "Nice work! All checks have passed."
