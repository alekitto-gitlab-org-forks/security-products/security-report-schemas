import {merge} from '../../support/merge'

const defaults = {
  analyzer: {
    id: 'test_analyzer_id',
    name: 'Test Analyzer name',
    vendor: {
      name: "Test Vendor"
    },
    version:"0.1.0"
  },
  scanner: {
    id: 'kubesec',
    name: 'Kubesec',
    url: 'https://github.com/controlplaneio/kubesec',
    vendor: {
      name: 'GitLab'
    },
    version: '2.6.0'
  },
  primary_identifiers: [
    {
      type: 'kubesec_rule_id',
      name: 'Kubesec Rule ID containers[] .resources .limits .cpu',
      value: 'Enforcing CPU limits prevents DOS via resource exhaustion'
    }
  ],
  type: 'sast',
  start_time: '2020-09-18T20:06:51',
  end_time: '2020-09-18T20:07:01',
  status: 'success'
}

export const scan = (scanOverrides) => {
  return merge(defaults, scanOverrides)
}
