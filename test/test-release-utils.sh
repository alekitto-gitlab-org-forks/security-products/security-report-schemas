#!/bin/bash

PROJECT_DIR=$(realpath "$(dirname "$(realpath "${BASH_SOURCE[0]}")")/..")

source "$PROJECT_DIR/scripts/changelog-utils.sh"
source "$PROJECT_DIR/scripts/release-utils.sh"

test_build_release_json_payload() {
  PROJECT_URL=${CI_PROJECT_URL:-"https://gitlab.com/gitlab-org/security-products/security-report-schemas"}

  VERSION="$(changelog_last_version)"
  CHANGELOG_DESCRIPTION=$(changelog_last_description)
  build_release_json_payload "$VERSION" "$CHANGELOG_DESCRIPTION" "$PROJECT_URL"

  assert_equals 0, "$?", "Failed to create build_release_json_payload"
}
