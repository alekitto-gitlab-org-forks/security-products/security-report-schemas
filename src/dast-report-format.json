{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/raw/master/dist/dast-report-format.json",
  "title": "Report format for GitLab DAST",
  "description": "This schema provides the the report format for Dynamic Application Security Testing (https://docs.gitlab.com/ee/user/application_security/dast).",
  "allOf": [
    { "$ref": "security-report-format.json" },
    {
      "properties": {
        "scan": {
          "properties": {
            "type": {
              "enum": ["dast", "api_fuzzing"]
            }
          }
        }
      }
    },
    {
      "properties": {
        "scan": {
          "required": ["scanned_resources"],
          "properties": {
            "scanned_resources": {
              "type": "array",
              "description": "The attack surface scanned by DAST.",
              "items": {
                "type": "object",
                "required": [
                  "method",
                  "url",
                  "type"
                ],
                "properties": {
                  "method": {
                    "type": "string",
                    "minLength": 1,
                    "description": "HTTP method of the scanned resource.",
                    "examples": ["GET", "POST", "HEAD"]
                  },
                  "url": {
                    "type": "string",
                    "minLength": 1,
                    "description": "URL of the scanned resource.",
                    "examples": ["http://my.site.com/a-page"]
                  },
                  "type": {
                    "type": "string",
                    "minLength": 1,
                    "description": "Type of the scanned resource, for DAST, this must be 'url'.",
                    "examples": ["url"]
                  }
                }
              }
            }
          }
        },
        "vulnerabilities": {
          "items": {
            "properties": {
              "evidence": {
                "type": "object",
                "properties": {
                  "source": {
                    "type": "object",
                    "description": "Source of evidence",
                    "required": [
                      "id",
                      "name"
                    ],
                    "properties": {
                      "id": {
                        "type": "string",
                        "minLength": 1,
                        "description": "Unique source identifier",
                        "examples": [
                          "assert:LogAnalysis",
                          "assert:StatusCode"
                        ]
                      },
                      "name": {
                        "type": "string",
                        "minLength": 1,
                        "description": "Source display name",
                        "examples": [
                          "Log Analysis",
                          "Status Code"
                        ]
                      },
                      "url": {
                        "type": "string",
                        "description": "Link to additional information",
                        "examples": [
                          "https://docs.gitlab.com/ee/development/integrations/secure.html"
                        ]
                      }
                    }
                  },
                  "summary": {
                    "type": "string",
                    "description": "Human readable string containing evidence of the vulnerability.",
                    "examples": [
                      "Credit card 4111111111111111 found",
                      "Server leaked information nginx/1.17.6"
                    ]
                  },
                  "request": {
                    "$ref": "#/definitions/http_request"
                  },
                  "response": {
                    "$ref": "#/definitions/http_response"
                  },
                  "supporting_messages": {
                    "type": "array",
                    "description": "Array of supporting http messages.",
                    "items": {
                      "type": "object",
                      "description": "A supporting http message.",
                      "required": [
                        "name"
                      ],
                      "properties": {
                        "name": {
                          "type": "string",
                          "minLength": 1,
                          "description": "Message display name.",
                          "examples": [
                            "Unmodified",
                            "Recorded"
                          ]
                        },
                        "request": {
                          "$ref": "#/definitions/http_request"
                        },
                        "response": {
                          "$ref": "#/definitions/http_response"
                        }
                      }
                    }
                  }
                }
              },
              "location": {
                "$ref": "#/definitions/location"
              },
              "assets": {
                "type": "array",
                "description": "Array of build assets associated with vulnerability.",
                "items": {
                  "type": "object",
                  "description": "Describes an asset associated with vulnerability.",
                  "required" : [
                    "type",
                    "name",
                    "url"
                  ],
                  "properties": {
                    "type": {
                      "type": "string",
                      "description": "The type of asset",
                      "enum": [ "http_session", "postman" ]
                    },
                    "name": {
                      "type": "string",
                      "minLength": 1,
                      "description": "Display name for asset",
                      "examples": [
                        "HTTP Messages",
                        "Postman Collection"
                      ]
                    },
                    "url": {
                      "type": "string",
                      "minLength": 1,
                      "description": "Link to asset in build artifacts",
                      "examples": [
                        "https://gitlab.com/gitlab-org/security-products/dast/-/jobs/626397001/artifacts/file//output/zap_session.data"
                      ]
                    }
                  }
                }
              }
            },
            "required": [ "location" ]
          }
        }
      }
    }
  ],
  "definitions": {
    "location": {
      "type": "object",
      "description": "Identifies the vulnerability's location.",
      "properties": {
        "hostname": {
          "type": "string",
          "description": "The protocol, domain, and port of the application where the vulnerability was found."
        },
        "method": {
          "type": "string",
          "description": "The HTTP method that was used to request the URL where the vulnerability was found."
        },
        "param": {
          "type": "string",
          "description": "A value provided by a vulnerability rule related to the found vulnerability. Examples include a header value, or a parameter used in a HTTP POST."
        },
        "path": {
          "type": "string",
          "description": "The path of the URL where the vulnerability was found. Typically, this would start with a forward slash."
        }
      }
    },
    "http_headers": {
      "type": "array",
      "description": "HTTP headers present on the request.",
      "items": {
        "type": "object",
        "required": ["name", "value"],
        "properties": {
          "name": {
            "type": "string",
            "minLength": 1,
            "description": "Name of the HTTP header.",
            "examples": ["Accept", "Content-Length", "Content-Type"]
          },
          "value": {
            "type": "string",
            "description": "Value of the HTTP header.",
            "examples": ["*/*", "560", "application/json; charset=utf-8"]
          }
        }
      }
    },
    "http_request": {
      "type": "object",
      "description": "An HTTP request.",
      "required": ["headers", "method", "url"],
      "properties": {
        "headers": {
          "$ref": "#/definitions/http_headers"
        },
        "method": {
          "type": "string",
          "minLength": 1,
          "description": "HTTP method used in the request.",
          "examples": ["GET", "POST"]
        },
        "url": {
          "type": "string",
          "minLength": 1,
          "description": "URL of the request.",
          "examples": ["http://my.site.com/vulnerable-endpoint?show-credit-card"]
        },
        "body": {
          "type": "string",
          "description": "Body of the request for display purposes. Body must be suitable for display (not binary), and truncated to a reasonable size.",
          "examples": [
            "user=jsmith&first=%27&last=smith"
          ]
        }
      }
    },
    "http_response": {
      "type": "object",
      "description": "An HTTP response.",
      "required": ["headers", "reason_phrase", "status_code"],
      "properties": {
        "headers": {
          "$ref": "#/definitions/http_headers"
        },
        "reason_phrase": {
          "type": "string",
          "description": "HTTP reason phrase of the response.",
          "examples": ["OK", "Internal Server Error"]
        },
        "status_code": {
          "type": "integer",
          "description": "HTTP status code of the response.",
          "examples": [200, 500]
        },
        "body": {
          "type": "string",
          "description": "Body of the response for display purposes. Body must be suitable for display (not binary), and truncated to a reasonable size.",
          "examples": [
            "{\"user_id\": 2}"
          ]
        }
      }
    }

  }
}
