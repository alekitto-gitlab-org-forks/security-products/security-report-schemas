## Summary

__Which field should be deprecated/removed? Why? Are there any available alternatives?__

## Checklist

All of the below would be considered breaking changes and must happen on the **next major release** of GitLab.

* [ ] Open a GraphQL deprecation issue if [Mutations::Vulnerabilities::Create](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/graphql/mutations/vulnerabilities/create.rb) uses the field.
* [ ] Open up an issue to remove the field from [VulnerabilityExports::Exporters::CsvService](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/app/services/vulnerability_exports/exporters/csv_service.rb) if it's being exported.

/epic https://gitlab.com/groups/gitlab-org/security-products/-/epics/6
/label ~"type::maintenance" ~"maintenance::removal" ~"group::threat insights"
