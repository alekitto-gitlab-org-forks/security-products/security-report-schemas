## Developer Certificate of Origin + License

By contributing to GitLab B.V., You accept and agree to the following terms and
conditions for Your present and future Contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., You reserve all right, title, and interest in and to
Your Contributions. All Contributions are subject to the following DCO + License
terms.

[DCO + License](https://gitlab.com/gitlab-org/dco/blob/master/README.md)

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Issue tracker

To get support for your particular problem please use the
[getting help channels](https://about.gitlab.com/getting-help/).

The [GitLab issue tracker on GitLab.com][tracker] is the right place for bugs and feature proposals about Security Products.
Please use the ~"devops::secure" label when opening a new issue to ensure it is quickly reviewed by the right people.

**[Search the issue tracker][tracker]** for similar entries before
submitting your own, there's a good chance somebody else had the same issue or
feature proposal. Show your support with an award emoji and/or join the
discussion.

Not all issues will be addressed and your issue is more likely to
be addressed if you submit a merge request which partially or fully solves
the issue. If it happens that you know the solution to an existing bug, please first
open the issue in order to keep track of it and then open the relevant merge
request that potentially fixes it.

[tracker]: https://gitlab.com/gitlab-org/gitlab/issues

## Becoming a maintainer

We're thrilled that you've decided to take your contribution to the next level! Here's how to become a maintainer:

1. Read the [How to become a project maintainer](https://about.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer) handbook section.
1. (optional) If you want to increase the chance of Dangerbot recommending you for a review, change your profile entry to list yourself as a `trainee_maintainer` of `security-report-schemas` ([example](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107232/diffs#c313a2ce902ce8c4058df52ea7057b94368b34da_14_13)).
1. Once your maintainer status is approved:
   1. Change your profile entry to list yourself as a `maintainer` of `security-report-schemas` ([example](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/114288/diffs)).
   1. Ask a current maintainer to add you as a member of [`gitlab-org/maintainers/security-report-schemas`](https://gitlab.com/groups/gitlab-org/maintainers/security-report-schemas/-/group_members?with_inherited_permissions=exclude).
