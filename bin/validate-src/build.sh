#!/bin/sh

cd "$(dirname "$0")" || exit 1

GOOS=linux go build -o ../validate-linux main.go
GOOS=darwin go build -o ../validate-mac main.go
